var express = require('express'),
    vehicles = require('./lib/cars.js'),
    credentials = require('./lib/credentials.js'),
    Users = require('./models/user.js'),
    Cars = require('./models/car.js'),
    Refuels = require('./models/refuel.js');

var app = express();

function sleep(time, callback) {
    var stop = new Date().getTime();
    while(new Date().getTime() < stop + time) {}
    callback();
}

// setup handlebars view engine

var handlebars = require('express3-handlebars')
  .create({ 
    defaultLayout: 'main',
    helpers: {
          static: function(name) {
              return require('./lib/static.js').map(name);
          }
      }
  });
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

if( app.thing === null ) console.log ( 'bleat');

// database configuration
var mongoose = require('mongoose');
var options = {
    server: {
       socketOptions: { keepAlive: 1 } 
    }
};
switch(app.get('env')){
    case 'development':
        mongoose.connect(credentials.mongo.development.connectionString, options);
        break;
    case 'production':
        mongoose.connect(credentials.mongo.production.connectionString, options);
        break;
    default:
        throw new Error('Unknown execution environment: ' + app.get('env'));
}

Users.find(function(err, users){
  if(users.length) return;

  new Users({
    login: 'admin',
    password: 'm',
  }).save();

  new Users({
    login: 'luk',
    password: 'l',
  }).save();
});

Cars.find(function(err, cars){
  if(cars.length) return;

  new Cars({
    owner: '549fbc6707803ec484653a7a',
    manufacturer: 'Audi',
    model: 'A4 B5',
    engine: '2.0',
    distance: 235789,
  }).save();

  new Cars({
    owner: '549fbc6707803ec484653a7a',
    manufacturer: 'Opel',
    model: 'Corsa D',
    engine: '1.4',
    distance: 45000,
  }).save();

});

Refuels.find(function(err, refuels){
  if(refuels.length) return;

  new Refuels({
    user: '549fbc6707803ec484653a7a',
    car: '549fca166f19cfcf90c3792c',
    date: new Date("2014-12-21"),
    fuel: "PB 95",
    liters: 1545,
    costPerLiter: 456,
    distance: 170000
  }).save();

  new Refuels({
    user: '549fbc6707803ec484653a7a',
    car: '549fca166f19cfcf90c3792c',
    date: new Date("2014-12-27"),
    fuel: "PB 95",
    liters: 1710,
    costPerLiter: 442,
    distance: 170500
  }).save();
});

// setup cars database
//var db = new vehicles();
var fortune = require('./lib/fortune.js');


app.set('port', process.env.PORT || 3000);

app.use(express.static(__dirname + '/public'));
app.use(require('cookie-parser')(credentials.cookieSecret));
app.use(require('express-session')());
app.use(require('body-parser')());
// tests

app.use(function(req, res, next){
  //console.log("testing");
  res.locals.showTests = app.get('env') !== 'production' && 
    req.query.test  === '1';
  next();
});

// common fetures
var static = require('./lib/static.js').map;

app.use(function(req, res, next){
  var now = new Date();
  res.locals.backgroundImage = now.getMonth()==11 && now.getDate()==23 ?
    static('/img/logo.jpg') :
    static('/img/bg-special.jpg');
  if(req.cookies.logged){
    res.locals.logged = true;
  }
  next();
});

// routing

app.get('/', function(req, res){
  res.redirect('/dashboard');
});

app.get('/about', function(req, res){
  res.render('about', {
    fortune: fortune.getFortune(),
    showHeaderLogin: true,
    styles: [static('/css/about.css')]
  });
});

//if( app.thing == null ) console.log( 'bleat!' );

app.get('/signin', function(req, res){
  res.locals.styles = [
    static('/css/signin.css')
  ];
  res.render('signin');
});

app.get('/dashboard', function(req, res){
  //res.cookie('monster', 'nom nom');
  //res.cookie('logged', false, {signed: true});
  console.log("GET: Dashboard for userid: " + req.cookies.logged);
  if(req.cookies.logged){
    Cars.find({owner: req.cookies.logged}, function(err, cars){
      var selectedCarIndex = req.query.car || 0;

      if(cars.length) {
        if(selectedCarIndex >= cars.length) {
          selectedCarIndex = 0;
        }

        res.cookie('carsLength', cars.length);

        var context = {
          cars: cars.map(function(car){
            return {
              _id: car._id,
              manufacturer: car.manufacturer,
              model: car.model,
              engine: car.engine,
              distance: car.distance,
            };
          })
        };

        for(var prop in context.cars) {
          context.cars[prop].carIndex = prop;
        }

        context.cars[selectedCarIndex].view = true;
        res.cookie('selectedCar', context.cars[selectedCarIndex]._id);
        

        context.styles = [static('/css/dashboard.css')];

        var selectedCar = context.cars[selectedCarIndex];

        context.dashboardTitle = 
          selectedCar.manufacturer + " " +
          selectedCar.model + " " +
          selectedCar.engine;

        Refuels.find({
            user: req.cookies.logged,
            car: selectedCar._id,
          }, 
          function(err, refuels){

            var min_distance = Number.MAX_VALUE;
            var max_distance = 0;
            var last_liters = 0;
            var sum_liters = 0;
            var sum_cost = 0;
            var avCost = 0;
            var avCostCount = 0;

            context.refuels = refuels.map(function(refuel){
              console.log(refuel.distance);
              if(min_distance > refuel.distance) {min_distance = refuel.distance;}
              if(max_distance < refuel.distance) {max_distance = refuel.distance;}
              if(refuel.liters) {sum_liters += refuel.liters;}
              if(refuel.costPerLiter) {
                sum_cost += (refuel.getLiters() * refuel.getCost());
                avCost += refuel.costPerLiter;
                avCostCount += 1;
              }
              last_liters = refuel.liters;
              return {
                _id: refuel._id,
                date: refuel.date.toDateString(),
                fuel: refuel.fuel,
                liters: refuel.getLiters(),
                costPerLiter: refuel.getCost(),
                distance: refuel.distance
              };
            });
          var totDist = max_distance-min_distance;
          context.totDistance = totDist > 0 ? totDist : 0;
          if(context.totDistance === 0) {
            context.avConsumption = 0;
          }
          else{
            context.avConsumption = ((sum_liters-last_liters)/context.totDistance).toFixed(2);
          }
          context.totCost = sum_cost.toFixed(2);
          context.avCost = (avCost/100/avCostCount).toFixed(2);

          console.log("Selected car: " + selectedCarIndex);
          console.log("Found cars: " + context.cars.length );
          console.log("Found refuels: " + context.refuels.length);
          
          res.render('dashboard', context); 

        });
      } 
      else {
        res.render('newcar', {
          styles: [static('/css/newcar.css')],
          dashboardTitle: "Please add your first car",
        });
      }
    });
  }
  else {
    console.log("Redirected to signin page");
    res.redirect('/signin');
  }
  
});

app.get('/newcar', function(req, res){
  if(req.cookies.logged){
    res.render('newcar', {
      styles: [static('/css/newcar.css')],
      dashboardTitle: "Enter the car parameters"
    });
  }
  else {
    console.log("Redirected to signin page");
    res.redirect('/signin');
  }
});

app.get('/vehicles/car', function(req, res){
  res.render('vehicles/car');
});

app.get('/vehicles/motorbike', function(req, res){
  res.render('vehicles/motorbike');
});

app.get('/vehicles/request-group-rate', function(req, res){
  res.render('vehicles/request-group-rate');
});

app.post('/process', function(req, res){
  switch(req.query.form){
    case 'login':
      console.log("POST: " + req.query.form);
      console.log(req.body.login + " " + req.body.password);

      var login = req.body.login;
      var pass = req.body.password;

      /*
      new Users({
        login: login,
        password: pass,
      }).save();
      */

      // retrieving from database
      /*Users.find(function(err, users){
        users.forEach(function(user){
          console.log("ID: " + user._id);
          console.log("Login: " + user.login);
          console.log("Password: " + user.password);
        });
      });*/

      Users.findOne({login: login, password: pass}, function(err, user){
        if(user) {
          res.cookie('logged', user._id);
          console.log("User " + user.login + " has logged properly.");
          res.redirect('/dashboard');
        }
        else {
          res.clearCookie('logged');
          console.log("Login failed");
          res.redirect('/signin');
        }
      });
    break;
    case 'logout':
      res.clearCookie('logged');
      res.redirect('/signin');
    break;
    case 'addRefuel':
      var date = req.body.date;
      var fuel = req.body.fuel;
      console.log("POST: " + req.query.form);
      console.log("Userid:" + req.cookies.logged);
      console.log("Carid:" + req.cookies.selectedCar);
      var refuel = new Refuels({
        user: req.cookies.logged,
        car: req.cookies.selectedCar,
        date: new Date(req.body.date),
        fuel: req.body.fuel,
        distance: req.body.distance
      });
      refuel.setLiters(req.body.liters);
      refuel.setCost(req.body.cost);
      refuel.save();
      console.log("Date:" + req.body.date);
      console.log("Fuel:" + req.body.fuel);
      console.log("Dist:" + req.body.distance);

      res.redirect(req.get('referer'));
    break;

    case 'delRefuel':
    console.log(req.body.id);
    Refuels.findOne({
        _id: req.body.id,
      }, 
      function(err, refuel){
        refuel.remove();
      }
    );

    res.redirect(req.get('referer'));

    break;

    case 'addCar':
      console.log("Added car");
      new Cars({
        owner: req.cookies.logged,
        manufacturer: req.body.manufacturer,
        model: req.body.model,
        engine: req.body.engine,
        distance: req.body.distance,
      }).save();

      if(req.cookies.carsLength){
        console.log("Cars length: " + req.cookies.carsLength);

        res.redirect('/dashboard?car=' + req.cookies.carsLength);
      }
      else{
        res.redirect('/dashboard');
      }
      
    break;

    case 'delCar':
      console.log("Remove car: " + req.cookies.selectedCar);
        Cars.findOne({
          _id: req.cookies.selectedCar,
        }, 
        function(err, car){
          car.remove();
        }
      );

      Refuels.find({
          car: req.cookies.selectedCar,
        }, 
        function(err, refuels){
          refuels.map(function(refuel){
            console.log("Remove refuel: " + refuel._id);
            refuel.remove();
          });
        }
      );

      res.clearCookie('selectedCar');
      sleep(2000, function(){
        res.redirect('/dashboard');
      });
    break;

    default:
      res.render('404');
  }
  
});

// custom 404 page
app.use(function(req, res, next){
  res.status(404);
  res.render('404');
});

app.use(function(err, req, res, next){
  res.status(500);
  //res.send('500 - Server Error');
  res.render('500');
});

app.listen(app.get('port'), function(){
  console.log('Express started on localhost:' + app.get('port') + '; press Ctrl-C to terminate.');
});