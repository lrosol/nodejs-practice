var mongoose = require('mongoose');

var refuelSchema = mongoose.Schema({
  user: mongoose.Schema.ObjectId,
  car: mongoose.Schema.ObjectId,
  date: Date,
  fuel: String,
  liters: Number,
  costPerLiter: Number,
  distance: Number
});

refuelSchema.methods.getLiters = function(){
  return (this.liters / 100).toFixed(2);
};

refuelSchema.methods.setLiters = function(liters){
  this.liters = liters * 100;
};

refuelSchema.methods.getCost = function(){
  return (this.costPerLiter / 100).toFixed(2);
};

refuelSchema.methods.setCost = function(costPerLiter){
  this.costPerLiter = costPerLiter * 100;
};

var Refuel = mongoose.model('Refuel', refuelSchema);
module.exports = Refuel;
