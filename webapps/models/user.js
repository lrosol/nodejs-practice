var mongoose = require('mongoose');

var usersSchema = mongoose.Schema({
  login: String,
  password: String,
});

var User = mongoose.model('User', usersSchema);
module.exports = User;