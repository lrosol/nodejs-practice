var mongoose = require('mongoose');

var carsSchema = mongoose.Schema({
  owner: mongoose.Schema.ObjectId,
  manufacturer: String,
  model: String,
  engine: String,
  distance: Number,
});

var Car = mongoose.model('Car', carsSchema);
module.exports = Car;