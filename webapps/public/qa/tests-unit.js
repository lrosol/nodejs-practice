var fortune = require('../../lib/fortune.js');
var expect = require('chai').expect;

suite('Fortune cookie tests', function(){
	test('getFortune() should return a fortune', function(){
		expect(typeof fortune.getFortune() === 'string');
	});

	test('getFortune() should return non zero length fortune', function(){
		expect(fortune.getFortune().length > 0);
	});
});